<?php
// SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-License-Identifier: CC0-1.0

// Generate locale requirement
\Locale::setDefault('en');

$localeRequirements = [];

foreach (\ResourceBundle::getLocales('') as $name) {
    if ($name === 'ca@valencia') {
        $name = 'ca_valencia';
    }
    $localeRequirements[] = $name;
}

$container->setParameter('app.locales', implode('|', $localeRequirements));

$container->setParameter('app.translations', [
"ca",
"ca_valencia",
"ca@valencia",
"cs",
"de",
"el",
"en_GB",
"es",
"eu",
"fr",
"it",
"nl",
"nn",
"pl",
"pt_BR",
"pt",
"ru",
"sk",
"sl",
"sv",
"uk",
]);
