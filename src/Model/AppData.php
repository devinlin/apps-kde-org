<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 * SPDX-FileCopyrightText: 2018 Daniel Laidig <d.laidig@gmx.de>
 * SPDX-FileCopyrightText: 2019 Harald Sitter <sitter@kde.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/**
* Get an array containing information for an application page on kde.org and gives
* access to its data.
* Written by Daniel Laidig <d.laidig@gmx.de>
* Rebased on new backend data by Harald Sitter <sitter@kde.org>
* ?>
*/

namespace App\Model;

use App\Model\IconType;
use App\Model\AppDataType;

/**
 * Detect if $haystack end with $needle
 * https://stackoverflow.com/questions/834303/startswith-and-endswith-functions-in-php/834355#834355
 * @param string $haystack
 * @param string $needle
 * @return bool
 */
function startsWith(string $haystack, string $needle): bool
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

/**
 * Detect if $haystack end with $needle
 * https://stackoverflow.com/questions/834303/startswith-and-endswith-functions-in-php/834355#834355
 * @param $haystack
 * @param $needle
 * @return bool
 */
function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}

/**
 * Class AppData
 */
class AppData
{
    /** @var string */
    private $id;

    /** @var array */
    private $raw;

    /** @var AppDataType */
    private $type;
    private $releases;
    private $screenshots;

    public function __construct($id, $data, $type)
    {
        $this->id = $id;
        $this->raw = $data;
        $this->type = $type;
        $this->screenshots = null;
    }

    public static function fromName($applicationName): ?AppData
    {
        $name = preg_replace('/[^a-z\-_0-9\.]/i', '', $applicationName);
        $filePath = './appdata/' . $name . '.json';

        // Make sure to resolve symlinks to their actual name. Specifically
        // symlinks may be used for backwards compatibility of old names, but
        // data resolution should happen on the current name.
        set_error_handler(function() { /* ignore errors */ });
        $link = readlink($filePath);
        restore_error_handler();

        if ($link) {
            $filePath = $link;
            $name = basename($filePath, '.json');
        }

        if (!file_exists($filePath)) {
            return null;
        }

        $data = json_decode(file_get_contents($filePath), true);

        $extensionFile = '../appdata-extensions/' . $name. '.json';
        if (file_exists($extensionFile)) {
            $data = array_merge_recursive($data, json_decode(file_get_contents($extensionFile), true));
        }

        if ($data['Type'] === 'desktop-application') {
            return new AppData($name, $data, AppDataType::Application);
        } else if ($data['Type'] === 'addon') {
            return new AppData($name, $data, AppDataType::Addon);
        } else if ($data['Type'] === 'console-application') {
            return new AppData($name, $data, AppDataType::Console);
        }
        return null;
    }

    public static function fromBinary($applicationBinary): ?AppData
    {
        $files = scandir('./appdata/');

        $filePath = NULL;
        foreach ($files as $file) {
            if (strpos($file, $applicationBinary)) {
                $filePath = './appdata/' . $file;
            }
        }

        if (!$filePath) {
            return NULL;
        }

        // Make sure to resolve symlinks to their actual name. Specifically
        // symlinks may be used for backwards compatibility of old names, but
        // data resolution should happen on the current name.
        set_error_handler(function() { /* ignore errors */ });
        $link = readlink($filePath);
        restore_error_handler();

        if ($link) {
            $filePath = $link;
            $name = basename($filePath, '.json');
        }

        if (!file_exists($filePath)) {
            return null;
        }

        $data = json_decode(file_get_contents($filePath), true);

        $extensionFile = '../appdata-extensions/' . $name. '.json';
        if (file_exists($extensionFile)) {
            $data = array_merge_recursive($data, json_decode(file_get_contents($extensionFile), true));
        }

        if ($data['Type'] === 'desktop-application') {
            return new AppData($name, $data, AppDataType::Application);
        } else if ($data['Type'] === 'addon') {
            return new AppData($name, $data, AppDataType::Addon);
        } else if ($data['Type'] === 'console-application') {
            return new AppData($name, $data, AppDataType::Console);
        }
        return null;
    }

    public function getType()
    {
        return $this->type;
    }

    /**
     * Same as AppStreamId but with .desktop stripped to force consistent naming.
     * @return string
     */
    public function getId()
    {
        if (empty($this->id)) {
            return $this->raw['X-KDE-ID'];
        }
        return $this->id;
    }

    /**
     * Same as getID but with org.kde stripped
     * @return string
     */
    public function getSimplifiedId()
    {
        if (empty($this->id)) {
            return substr($this->raw['X-KDE-ID'], 8);
        }
        return substr($this->id, 8);
    }

    /**
     * Get application name
     * @return array|null
     */
    public function getName(): ?array
    {
        return $this->raw['Name'];
    }

    /**
     * Get generic name
     * @return array|null
     */
    function getGenericName(): ?array
    {
        return $this->raw['X-KDE-GenericName'];
    }

    /**
     * Get AppStream summary
     * @fixme summary is a bad fit for this as it can be rather long
     * @return string|null
     */
    public function getSummary(): ?array
    {
        return $this->raw['Summary'];
    }

    public function getPrimaryCategory(): string
    {
        if (isset($this->raw['Categories'][0])) {
            return $this->raw['Categories'][0];
        }
        return 'Unmaintained';
    }

    public function getCategories(): ?array
    {
        return $this->raw['Categories'];
    }

    public function getDescription()
    {
        return $this->raw['Description'];
    }

    /**
     * @return string|null
     */
    public function getLicenseIdentifier(): ?string
    {
        if (isset($this->raw['ProjectLicense'])) {
            return $this->raw['ProjectLicense'];
        }
        return null;
    }

    public function getAuthors(): ?array
    {
        if (isset($this->raw['authors'])) {
            return $this->raw['authors'];
        }
        return null;
    }

    /**
     * Get the application releases.
     * @return array Return an array of releases, if no release is present return an empty array.
     */
    public function getReleases(): ?array {
        // implement basic result caching
        if ($this->releases !== null) {
            return $this->releases;
        }

        $this->releases = [];

        if (!array_key_exists('Releases', $this->raw)) {
            return $this->releases;
        }

        foreach($this->raw['Releases'] as $release) {
            $release = Release::fromData($release);
            if ($release) {
                $this->releases[] = $release;
            }
        }

        return $this->releases;
    }

    /**
     * Get the latest stable application release.
     * @return Release Latest release or null if not present
     */
    public function getLatestStableRelease(): ?Release
    {
        return $this->getLatestRelease(ReleaseType::Stable);
    }

    /**
     * Get the latest development application release.
     * @return Release Latest release or null if not present
     */
    public function getLatestDevelopmentRelease(): ?Release
    {
        return $this->getLatestRelease(ReleaseType::Development);
    }

    private function getLatestRelease(int $releaseType): ?Release
    {
        $releases = $this->getReleases();
        $releases = array_filter(
            $releases,
            function($release) use ($releaseType) {
                return $release->getType() == $releaseType;
            }
        );
        if ($releases) {
            usort($releases, function($a, $b) { return $b->getTimestamp() - $a->getTimeStamp(); });
            return $releases[0];
        }
        return null;
    }

    public function getCredits(): ?array
    {
        if (isset($this->raw['credits'])) {
            return $this->raw['credits'];
        }
        return null;
    }

    /**
     * Get the screenshots.
     * @return array Return an array of screenshots, if no screenshot is present return an empty array.
     *               Each items contains a thumbnails (optional), a caption (optional) and a source-image.
     * @see https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-screenshots
     */
    public function getScreenshots(): ?array {
        // implement basic result caching
        if ($this->screenshots !== null) {
            return $this->screenshots;
        }

        $this->screenshots = [];

        if (!array_key_exists('Screenshots', $this->raw)) {
            return $this->screenshots;
        }

        $lang = 'C';

        foreach($this->raw['Screenshots'] as $screenshot) {
            $screenshot = Screenshot::fromData($screenshot);
            if ($screenshot) {
                $this->screenshots[] = $screenshot;
            }
        }

        return $this->screenshots;
    }

    public function licenseHtml() {
        // TODO: we should really render SPDX properly. There are some databases
        //   of licenses and their texts. Or we could send URLs to spdx.org.
        $license = $this->raw['ProjectLicense'];
        $text = '<p>';
        // $stripped_license = str_replace("+", "", $license);
        // $text = i18n_var('%1 is distributed under the terms of the <script type="text/javascript">document.write(spdxToHTML("%3"))</script></a>.',
        //                  $this->name(), $stripped_license, $license);
        switch($license) {
        case 'GPL-2.0+': case 'GPL-2.0': case 'GPL-2.0-or-later': case 'GPL-2.0-only':
            $text .= i18n_var('%1 is distributed under the terms of the <a itemprop"license" href="https://www.gnu.org/licenses/old-licenses/gpl-2.0.html">GNU General Public License (GPL), Version 2</a>.', $this->name());
            break;
        case 'GPL-3.0+': case 'GPL-3.0': case 'GPL-3.0-or-later': case 'GPL-3.0-only':
            $text .= i18n_var('%1 is distributed under the terms of the <a itemprop="license" href="https://www.gnu.org/licenses/gpl.html">GNU General Public License (GPL), Version 3</a>.', $this->name());
            break;
        case 'LGPL-2.0-only': case 'LGPL-2.0-or-later': case 'LGPL-2.1-only': case 'LGPL-2.1-or-later': case 'LGPL':
            $text .= i18n_var('%1 is distributed under the terms of the <a itemprop="license" href="https://www.gnu.org/licenses/old-licenses/lgpl-2.1.html"> GNU Lesser General Public License, version 2</a>.', $this->name());
            break;
        case 'LGPL-3.0-only': case 'LGPL-3.0-or-later': case 'LGPL-3.0':
            $text .= i18n_var('%1 is distributed under the terms of the <a itemprop="license" href="https://www.gnu.org/licenses/lgpl.html"> GNU Library General Public License, version 3</a>.', $this->name());
            break;
        default:
            $text .= $license.'.';
        }
        $text .= '</p>';
        return $text;
    }

    /**
     * Get the homepage iff exist and is not located in kde.org
     * @return string|null
     */
    public function getHomepage(): ?string
    {
        if (!isset($this->raw['Url']['homepage'])) {
            return null;
        }
        $homepage = $this->raw['Url']['homepage'];

        if (startsWith($homepage, 'https://www.kde.org')
            || startsWith($homepage, 'https://kde.org')
            || startsWith($homepage, 'http://www.kde.org')
            || startsWith($homepage, 'http://kde.org')) {
            return null;
        }

        return $homepage;
    }

    public function getAppStreamId()
    {
        return $this->raw['ID'];
    }

    public function getIconType()
    {
        if (isset($this->raw['Icon']['local'])) {
            return IconType::Local;
        } else if (isset($this->raw['Icon']['stock'])) {
            return IconType::Stock;
        }
        return IconType::None;
    }

    public function getIcon()
    {
        $iconType = $this->getIconType();

        switch ($iconType) {
        case IconType::Local:
            return $this->raw['Icon']['local'][0]['name'];
            break;

        case IconType::Stock:
            $iconName = $this->raw['Icon']['stock'];

            // TODO find icon path

            break;

        case IconType::None:
            // TODO
            break;

        default:
            throw new \Exception("Should not happen, IconType is undefined");
        }
    }

    /**
     * @return mixed
     * FIXME: not in appdata
     */
    public function getKdeAppsId(): ?string
    {
        return $this->raw['kde-apps.org'];
    }

    /**
     * @return string
     * FIXME: not in appdata
     */
    public function getUserbase()
    {
        if (isset($this->raw['userbase'])) {
            return "https://userbase.kde.org/" . $this->raw['userbase'];
        }
        return null;
    }

    /**
     * @see https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-url
     * @return string|null handbook iff exist otherwise null
     */
    public function getHandbook(): ?string {
        if (isset($this->raw['Url']) && isset($this->raw['Url']['help'])) {
            return $this->raw['Url']['help'];
        }

        return null;
    }


    /**
     * Get AppData extension X-KDE-Forum
     * @return string
     */
    public function getForum(): string
    {
        if (isset($this->raw['Custom']) && isset($this->raw['Custom']['KDE::forum'])) {
            return 'https://forum.kde.org/viewforum.php?f='.$this->raw['Custom']['KDE::forum'];
        } else if (isset($this->raw['X-KDE-Forum'])) {
            return 'https://forum.kde.org/viewforum.php?f='.$this->raw['X-KDE-Forum'];
        } else {
            return 'https://forum.kde.org/';
        }
    }

    /**
     * Get Application irc channel
     * @return string[]
     */
    public function getIrcChannels(): array
    {
        if (isset($this->raw['Custom']) && isset($this->raw['Custom']['KDE::irc'])) {
            return [$this->raw['Custom']['KDE::irc']];
        } else if (isset($this->raw['X-KDE-IRC'])) {
            $irc = $this->raw['X-KDE-IRC'];
            if (is_array($irc)) {
                return $irc;
            } else {
                return [$irc];
            }
        } else {
            return ["#kde"];
        }
    }

    /**
     * Get Application matrix channel
     * @return string[]
     */
    public function getMatrixChannel(): ?string
    {
        if (isset($this->raw['Custom']) && isset($this->raw['Custom']['KDE::matrix'])) {
            return $this->raw['Custom']['KDE::matrix'];
        }
        return null;
    }

    private function unifyMailingList(string $mailingList): string
    {
        if (endsWith($mailingList, '@kde.org')) {
            $mailingList = substr($mailingList, 0, -8);
        }

        if (startsWith($mailingList, 'mailto:')) {
            $mailingList = substr($mailingList, 0, 7);
        }

        if (startsWith($mailingList, 'https://mail.kde.org/mailman/listinfo/')) {
            $mailingList = substr($mailingList, 0, 38);
        }

        return $mailingList;
    }

    /**
     * AppData extension X-KDE-MailingLists or KDE::mailinglist
     * @return string[]
     */
    public function getMailingLists(): array
    {
        if (isset($this->raw['Custom']) && isset($this->raw['Custom']['KDE::mailinglist'])) {
            return [$this->unifyMailingList($this->raw['Custom']['KDE::mailinglist'])];
        }
        $ml = [];
        if (isset($this->raw['X-KDE-MailingLists'])) {
            $ml = $this->raw['X-KDE-MailingLists'];
            if (!is_array($ml)) {
                $ml = [$ml];
            }
        } else {
            $ml = ["kde@kde.org"];
        }

        return array_map('App\Model\AppData::unifyMailingList', $ml);
    }

    /**
     * AppData extension KDE::mailinglist-devel
     * @return string[]
     */
    public function getDevelMailingList(): ?string
    {
        if (isset($this->raw['Custom']) && isset($this->raw['Custom']['KDE::mailinglist-devel'])) {
            return $this->unifyMailingList($this->raw['Custom']['KDE::mailinglist-devel']);
        }
        return null;
    }

    /**
     * AppData extension X-KDE-Repository
     * @return string|null
     */
    public function repository(): ?string
    {
        return $this->raw['X-KDE-Repository'];
    }

    /**
     * @return string the application bugtracker (external or KDE bugzilla)
     */
    public function getBugTracker(): ?string
    {
        if (isset($this->raw['Url']) && isset($this->raw['Url']['bugtracker'])) {
            return $this->raw['Url']['bugtracker'];
        }
        return null;
    }

    /**
     * Get application FAQ
     * @return string|null
     */
    public function getFaq(): ?string
    {
        if (isset($this->raw['Url']) && isset($this->raw['Url']['faq'])) {
            return $this->raw['Url']['faq'];
        }
        return null;
    }

// FIXME: not implemented url types in frontend:
// faq
// donation
// translate

    public function getWindowsStoreLink(): ?string
    {
        if (isset($this->raw['Custom']) && isset($this->raw['Custom']['KDE::windows_store'])) {
            return $this->raw['Custom']['KDE::windows_store'];
        }
        return null;
    }

    public function getFDroidLink(): ?string
    {
        if (isset($this->raw['Custom']) && isset($this->raw['Custom']['KDE::f_droid'])) {
            return $this->raw['Custom']['KDE::f_droid'];
        }
        return null;
    }

    public function getGooglePlayLink(): ?string
    {
        if (isset($this->raw['Custom']) && isset($this->raw['Custom']['KDE::google_play'])) {
            return $this->raw['Custom']['KDE::google_play'];
        }
        return null;
    }

    // TODO: ebn is broken for now
    public function hasEbn() {
        return false;
    }

    public function getEbnCodeCheckingUrl()
    {
        return 'http://ebn.kde.org/krazy/reports/'.$this->ebnUrlBase().'/index.html';
    }

    public function getEbnDocCheckingUrl()
    {
        return 'http://ebn.kde.org/sanitizer/reports/'.$this->ebnUrlBase().'/index.html';
    }

    private function ebnUrlBase() {
        // examples:
        // KDE/kdeedu/parley:               http://englishbreakfastnetwork.org/krazy/reports/kde-4.x/kdeedu/parley/index.html
        // KDE/kdebase/apps/dolphin:        http://englishbreakfastnetwork.org/krazy/reports/kde-4.x/kdebase-apps/dolphin/index.html
        // extragear/graphics/digikam:      http://englishbreakfastnetwork.org/krazy/reports/extragear/graphics/digikam/index.html
        // koffice/kword:                   http://englishbreakfastnetwork.org/krazy/reports/bundled-apps/koffice/kword/index.html
        // amarok (gitorious):              http://englishbreakfastnetwork.org/krazy/reports/extragear/multimedia/amarok-git/index.html

        $ebn = '';
        // extract path segments from module parent
        $parts = explode('/', $this->raw['parent']);

        // replace path segment by "kde-4.x" for "kde"
        if ($parts[0] == 'kde') {
            $ebn = "kde-4.x/" . $parts[1] . "/" . strtolower($this->raw['repository'][1]);
        } else {
            $ebn = $parts[0] . "/" . $parts[1] . "/" . strtolower($this->name());
        }
        return $ebn;
    }
}
